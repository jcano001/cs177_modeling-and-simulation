/*
  Justin Cano
  860945660
  CS 177 Winter 2014
  I hereby certify that all of the work done in this submission is entirely my own.
*/
#include <iostream>
#include <stdlib.h>
#include <cpp.h>			/*include the CSIM C++ header file*/
#include <string.h>
#include <math.h>
#include <iomanip>

const double profit = .025; // dollars per litre
const double cost = 20;

using namespace std;

int NUM_PUMPS;
double ARRIVAL_STREAM, LITRE_STREAM, BALKING_STREAM, SERVICE_STREAM,
	REPORT_INTERVAL, ENDING_TIME;

/*stat variables*/
int TOTAL_ARRIVALS = 0;
int CUSTOMERS_SERVED = 0;
int BALKING_CUSTOMERS = 0;
double TOTAL_LITRES_SOLD = 0.0;
double TOTAL_LITRES_MISSED = 0.0;
double TOTAL_WAITING_TIME = 0.0;
double TOTAL_EMPTY_QUEUE_TIME = 0.0;
double TOTAL_SERVICE_TIME = 0.0;

facility_ms * pumpStand;
facility * pumpStandDiesel;
event_set * pump_is_free;

double maxPumpStandQueueLength = 0;
double maxPumpStandDieselQueueLength = 0;

void arrivals();			/*function declarations*/
void arr_car();
void stats();

bool doesCarBalk( double litres, double queueLength )
{
	double num = uniform(0,BALKING_STREAM); 
	return queueLength > 0 && num > (40 + litres) / (25 * (3 + queueLength));
}

bool doesCarNeedDiesel()
{
	double num = uniform(0,BALKING_STREAM);
	return num <= 0.05;
}

double serviceTime( double litres )
{
	double howLong = -6;

	for (int i = 1; i <= 12; i++)
		howLong += uniform(0,SERVICE_STREAM);

	return 150 + 0.5 * litres + 30 * howLong;
}

extern "C" void sim() 			/*sim process*/
{
	create("sim");			/*make this a process*/

	cout << "Enter Report Interval time: ";
	cin >> REPORT_INTERVAL;

	cout << "Enter Ending time: ";
	cin >> ENDING_TIME;

	cout << "Enter number of pumps: ";
	cin >> NUM_PUMPS;
	pumpStand = new facility_ms("pumpStand",NUM_PUMPS);	/*customer queue at pumpStand*/
	pumpStandDiesel = new facility("pumpStandDiesel");
	pump_is_free = new event_set("pump is free",NUM_PUMPS);
	cout << "This simulation run uses " << NUM_PUMPS << " pumps with 1 diesel pump\n";

	cout << "Enter random number seeds:" << endl << "Arrival Stream: ";
	cin >> ARRIVAL_STREAM;
	cout << "Litre Stream: ";
	cin >> LITRE_STREAM;
	cout << "Balking Stream: ";
	cin >> BALKING_STREAM;
	cout << "Service Stream: ";
	cin >> SERVICE_STREAM;

	cout << endl << endl << endl;

	for( int i = 0; i < NUM_PUMPS; i++ )	/*initially set all pumps to be free*/
		(*pump_is_free)[i].set();

	arrivals();			/*start a stream of arriving cars*/	
	cout << setw(8) << "Current" << setw(8) << "Total" << setw(10) << "NoQueue" << setw(8) << "Car" << setw(8) << "Average" 
		<< setw(7) << "Number" << setw(8) << "Average" << setw(8) << "Pump" << setw(8) << "Total"
		<< setw(8) << "Lost" << setw(5) << "Max" << endl;
	cout << setw(8) << "Time" << setw(8) << "Cars" << setw(10) << "Fraction" << setw(8) << "Time" << setw(8) << "Litres" 
		<< setw(7) << "Balked" << setw(8) << "Wait" << setw(8) << "Usage" << setw(8) << "Profit" 
		<< setw(8) << "Profit" << setw(5) << "Size" << endl;
	for(int i = 0; i < 86; i++)
		cout << "-";
	cout << endl;
	stats();
	hold(ENDING_TIME+REPORT_INTERVAL);			/*wait for 10000 minutes plus report interval (for stats)*/
}

void arrivals()
{
	create("arrivals");		/*create arriving cars in the background*/
	while(clock <= ENDING_TIME )
	{
		hold(uniform(0,ARRIVAL_STREAM));
		arr_car();		/*new car appears at gas station*/
	}
}


void arr_car()
{
	create("car");			/*make this a process*/
	TOTAL_ARRIVALS++;
	// TODO: determine if car balks
	double pumpStandQueueLength = pumpStand->qlength();
	double pumpStandDieselQueueLength = pumpStandDiesel->qlength();

	double simulationTime = clock;

	double litresNeeded = 10 + uniform(0,LITRE_STREAM) * 50;

	bool diesel = doesCarNeedDiesel();

	double arrivalTime = simulationTime;

	if( !doesCarBalk(litresNeeded,pumpStandQueueLength) )
	{
		if( pumpStandQueueLength == 0 && pumpStandDieselQueueLength == 0 )
			TOTAL_EMPTY_QUEUE_TIME += simulationTime;

		// TODO: determine if car goes into diesal or regular queue
		// car goes to diesel if car needs diesel gas or if 
		// diesel queue is shorter than regular queue
		if( diesel || pumpStandDieselQueueLength < pumpStandQueueLength )
		{
			pumpStandDiesel->reserve();	/*join the queue at pumpStandDiesel*/

			pumpStandDieselQueueLength = pumpStandDiesel->qlength(); /*update max queue length*/
			if( pumpStandDieselQueueLength > maxPumpStandDieselQueueLength )
				maxPumpStandDieselQueueLength = pumpStandDieselQueueLength;

			hold(uniform(0,SERVICE_STREAM));
			pumpStandDiesel->release();
			if( pumpStandQueueLength == 0 && pumpStandDieselQueueLength == 0 )
				TOTAL_EMPTY_QUEUE_TIME -= simulationTime;
			
		}
		else
		{
			pumpStand->reserve();		/*join the queue at pumpStand*/

			pumpStandQueueLength = pumpStand->qlength();			/*update max queue length*/
			if( pumpStandQueueLength > maxPumpStandQueueLength )
				maxPumpStandQueueLength = pumpStandQueueLength;

			for( int i = 0; i < NUM_PUMPS; i++ )
			{
				if( (*pump_is_free)[i].state()==OCC )
				{
					(*pump_is_free)[i].clear();	/*clear call; pump is NOT free*/
					hold(uniform(0,SERVICE_STREAM));
					pumpStand->release();
					if( pumpStandQueueLength == 0 && pumpStandDieselQueueLength == 0 )
						TOTAL_EMPTY_QUEUE_TIME -= simulationTime;
					(*pump_is_free)[i].set();	/*pump is now free*/
					break;
				}
			}
		}
		double waitTime = clock - arrivalTime;
		TOTAL_LITRES_SOLD += litresNeeded;
		CUSTOMERS_SERVED++;
		TOTAL_WAITING_TIME += waitTime;
		TOTAL_SERVICE_TIME += serviceTime(litresNeeded);
	}
	else
	{
		TOTAL_LITRES_MISSED += litresNeeded;
		BALKING_CUSTOMERS++;
	}	
}

void stats()
{
	create("stats");
	cout.precision(5);
	while(clock <= ENDING_TIME)
	{
		hold(REPORT_INTERVAL);
		double simulationTime = clock;
		double maxSize = maxPumpStandQueueLength;
		if( maxSize < maxPumpStandDieselQueueLength )
			maxSize = maxPumpStandDieselQueueLength;
		cout << setw(8) << simulationTime << setw(8) << TOTAL_ARRIVALS << setw(10) << TOTAL_EMPTY_QUEUE_TIME/simulationTime;
		if(TOTAL_ARRIVALS > 0)
			cout << setw(8) << simulationTime/TOTAL_ARRIVALS << setw(8) << (TOTAL_LITRES_SOLD+TOTAL_LITRES_MISSED)/TOTAL_ARRIVALS;
		else
			cout << setw(8) << "unknown" << setw(8) << "unknown";
		cout << setw(7) << BALKING_CUSTOMERS;
		if(CUSTOMERS_SERVED > 0)
			cout << setw(8) << TOTAL_WAITING_TIME/CUSTOMERS_SERVED;
		else
			cout << setw(8) << "unknown";
		cout << setw(8) << TOTAL_SERVICE_TIME/(NUM_PUMPS*simulationTime) << setw(8) << (TOTAL_LITRES_SOLD*profit-cost*NUM_PUMPS)
			<< setw(8) << TOTAL_LITRES_MISSED*profit << setw(5) << maxSize;

		cout << endl;
	}	
}
/* Output

Report Interval time: 1000
Ending Time: 10000
Number of pumps: 3
Arrival Stream: 2
Litre Stream: 8
Balking Stream: 2
Serviec Stream: 9

 Current   Total   NoQueue     Car Average Number Average    Pump   Total    Lost  Max
    Time    Cars  Fraction    Time  Litres Balked    Wait   Usage  Profit  Profit Size
--------------------------------------------------------------------------------------
    1000     942    1.8854  1.0616  203.45    105  7.7634  472.99  4523.3  208.01    5
    2000    1944    14.845  1.0288  203.99    248  7.8501  479.01  9359.5  494.17    5
    3000    2957    18.689  1.0145  205.18    401  8.2353  483.47   14280  827.38    5
    4000    3947    22.004  1.0134  207.59    542  8.3283  483.76   19286  1138.4    5
    5000    4958    21.124  1.0085  208.95    702  8.5263  484.16   24339  1501.2    6
    6000    5966    35.282  1.0057  208.04    844  8.3401  486.16   29239  1730.3    6
    7000    6966     45.25  1.0049  208.69    999  8.4195  485.44   34213  2069.4    6
    8000    7981    41.842  1.0024  208.77   1157  8.4559  486.42   39195  2399.9    6
    9000    8964    50.637   1.004  208.55   1280  8.3662   486.4   44061  2616.2    6
   10000    9957    54.921  1.0043  208.39   1427  8.3491  485.96   48894  2919.4    6
*/
