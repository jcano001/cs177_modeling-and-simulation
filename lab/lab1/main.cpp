#include <iostream>
#include <stdlib.h>
#include <vector>
#include <iomanip>
#include <cmath>
#include <fstream>

using namespace std;

const double PI = 3.14159;

/***** EXERCISE 1 FUNCTIONS *****/
bool in_circle(double & x, double & y) {
	double x_squared = x*x;
	double y_squared = y*y;

	if( (x_squared + y_squared) <= 1.0 )
		return true;

	return false;
}

double estimate_pi( const int & n ) {
	double x,y;
	double count = 0.00;

	vector< pair<double,double> > numbers;
	// PRNG
	for( int i = 0; i < n; i++ ) {
		x = rand();
		x = x/RAND_MAX;
		y = rand();
		y = y/RAND_MAX;

		numbers.push_back( make_pair(x,y) );

		if( in_circle(x,y) ) count++;
	}

	double pi = (count/n)*4;

	return pi;
}

/***** EXERCISE 2 FUNCTIONS *****/
double * pi_sequence(const int& n) {
	double x,y;
	double count = 0.00;
	double * array = new double[n];

	vector<pair< double,double> > numbers;
	// PRNG
	for( int i = 1; i <= n; i++ ) {
		x = rand();
		x = x/RAND_MAX;
		y = rand();
		y = y/RAND_MAX;

		numbers.push_back( make_pair(x,y) );

		if( in_circle(x,y) ) count++;

		double pi = (count/i)*4;
		array[i] = pi;
	}

	return array;
}

/***** EXERCISE 3 FUNCTIONS *****/
bool in_triangle( double & x, double & y ) {
	double inside = x + y;
	if( inside >= 1 ) return true;

	return false;
}

double * new_pi_sequence(const int& n) {
	double x,y;
	double triangle = 0.00;
	double lens = 0.00;
	double * array = new double[n];

	vector<pair< double,double> > numbers;
	// PRNG
	for( int i = 1; i <= n; i++ ) {
		x = rand();
		x = x/RAND_MAX;
		y = rand();
		y = y/RAND_MAX;

		if( in_triangle(x,y) ) {
			triangle++;
			if( in_circle(x,y) )
				lens++;
			double z = x + y;
			if( z < 1) {
				x = 1-x;
				y = 1-y;
			}
		}

		numbers.push_back( make_pair(x,y) );

		double pi = (2*(lens/triangle)) + 2;
		array[i] = pi;
	}

	return array;
}

int main() {
	srand(time(NULL));
	/***** EXERCISE 1 *****/
	int N = 1;
	for( int i = 0; i < 8; i++ ) {
		double estimate = estimate_pi(N);
		double difference = abs(estimate - PI);
		cout << setw(10) << N
			<< setw(10) << estimate
			<< setw(10) << setprecision(5) << difference << endl;
		N = N*10;
	}

	/***** EXERCISE 2 *****/
	ofstream ex2_file;
	ex2_file.open("sequences_ex2.csv");
	N = 500;
	for( int i = 0; i < 6; i++ ) {
		double * array = pi_sequence(N);

		for( int i = 0; i < N; i++ ) ex2_file << array[i] << ", ";

		ex2_file << endl;

	}
	ex2_file.close();

	/***** EXERCISE 3 *****/
	ofstream ex3_file;
	ex3_file.open("sequences_ex3.csv");
	N = 500;
	for( int i = 0; i < 6; i++ ) {
		double * array = new_pi_sequence(N);

		for( int i = 0; i < N; i++ ) ex3_file << array[i] << ", ";

		ex3_file << endl;

	}
	ex3_file.close();

	return 0;
}
