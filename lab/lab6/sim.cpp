/*
  Justin Cano
  860945660
  CS 177 Winter 2014
  I hereby certify that all of the work done in this submission is entirely my own.
*/
#include <iostream>
#include <cpp.h>			/*include the CSIM C++ header file*/
#include <string.h>
#include <math.h>
#include <fstream>

using namespace std;

const int NUM_ELEMENTS = 125;

const double MEAN = 42.96902364;
const double STANDARD_DEVIATION = 2.8604479234;

double U[NUM_ELEMENTS];


extern "C" void sim()
{
	ofstream file;
	file.open("normal_distribution.txt");
	for( int n = 0; n < NUM_ELEMENTS; n++ )
	{
		U[n] = normal(MEAN,STANDARD_DEVIATION);
		file << U[n] << endl;
	}
	file.close();
}
