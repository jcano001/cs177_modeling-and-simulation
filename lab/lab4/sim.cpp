/*
  Justin Cano
  860945660
  CS 177 Winter 2014
  I hereby certify that all of the work done in this submission is entirely my own.
*/
#include <iostream>
#include <cpp.h>			/*include the CSIM C++ header file*/
#include <string.h>
#include <math.h>


using namespace std;

const int NUM_ROWS = 10;
const int NUM_ELEMENTS = 10000;
const double C4 = 10000;
const double C5 = 10000000;
const double C6 = 10e9;


double U[NUM_ROWS][NUM_ELEMENTS];

double sampleMean( double * array )
{
	double sum = 0;

	for( int n = 0; n < NUM_ELEMENTS; n++ )
		sum += array[n];

	return sum/NUM_ELEMENTS;
}

double sampleVariance2( double * array )
{
	double mean = sampleMean( array );
	double sum = 0;

	for( int n = 0; n < NUM_ELEMENTS; n++ )
	{
		double result = array[n] - mean;
		sum += pow(result,2);
	}

	return sum/(NUM_ELEMENTS-1);
}

double sampleVariance1( double * array )
{
	double term1 = 0;
	double term2 = NUM_ELEMENTS*pow(sampleMean(array),2);

	for( int n = 0; n < NUM_ELEMENTS; n++ )
	{
		term1 += pow(array[n],2);
	}

	double result = term1 - term2;
	return result/(NUM_ELEMENTS-1);
}

void report( double * samples, double t )
{
	double mean = sampleMean(samples);
	double var_twice = sampleVariance2(samples);
	double var_once = sampleVariance1(samples);

	cout << "Mean: " << mean << endl
		<< "Variance_twice: " << var_twice << endl
		<< "Variance_once: " << var_once << endl;

	double s_once = sqrt(var_once/NUM_ELEMENTS);
	double s_twice = sqrt(var_twice/NUM_ELEMENTS);

	double lowerBounds = mean - (s_once*t);
	double upperBounds = mean + (s_once*t);
	cout << "[" << lowerBounds << "," << upperBounds << "]\n";

	lowerBounds = mean - (s_once*t);
	upperBounds = mean + (s_once*t);
	cout << "[" << lowerBounds << "," << upperBounds << "]\n";

	cout << endl;
}

extern "C" void sim()
{
	cout << endl << "**********" << endl << "==Step 3==" << endl;

	/***** Step 1  *****/
	for( int r = 0; r < NUM_ROWS; r++ )
	{
		for( int n = 0; n < NUM_ELEMENTS; n++ )
		{
			U[r][n] = uniform(0,1);
		}
	}

	/***** Step 2 & 3  *****/
	double t = 2.262;
	for( int i = 0; i < NUM_ROWS; i++ )
	{
		cout << "Row " << i+1 << ":\n";
		report(U[i],t);
	}

	cout << "**********" << endl << "==Step 4==" << endl;

	/***** Step 4  *****/
	for( int r = 0; r < NUM_ROWS; r++ )
	{
		for( int n = 0; n < NUM_ELEMENTS; n++ )
		{
			U[r][n] = uniform(0,1) + C4;
		}
	}

	for( int i = 0; i < NUM_ROWS; i++ )
	{
		cout << "Row " << i+1 << ":\n";
		report(U[i],t);
	}

	cout << "**********" << endl << "==Step 5==" << endl;

	/***** Step 5  *****/
	for( int r = 0; r < NUM_ROWS; r++ )
	{
		for( int n = 0; n < NUM_ELEMENTS; n++ )
		{
			U[r][n] = uniform(0,1) + C5;
		}
	}

	for( int i = 0; i < NUM_ROWS; i++ )
	{
		cout << "Row " << i+1 << ":\n";
		report(U[i],t);
	}

	cout << "**********" << endl << "==Step 6==" << endl;

	/***** Step 6  *****/
	for( int r = 0; r < NUM_ROWS; r++ )
	{
		for( int n = 0; n < NUM_ELEMENTS; n++ )
		{
			U[r][n] = uniform(0,1) + C6;
		}
	}

	for( int i = 0; i < NUM_ROWS; i++ )
	{
		cout << "Row " << i+1 << ":\n";
		report(U[i],t);
	}

	/***** Step 7 *****/
	/*
	 * In theory, by adding the large constant C in steps 4-6,
	 * the one-pass variance calculation method becomes unstable
	 * because of a cancellation error: subtracting (potentially 
	 * large) numbers from each other to produce a (much smaller)
	 * final answer may make the final answer negligable and therefore
	 * be reduced to zero.
	 *
	 * My observation in practice reflects what should happen in theory:
	 * the final answer that we produced becomes negligable, so now the only
	 * answers that appear for the one-pass variance calculation method are
	 * large whole numbers that do not show our desired (smaller) answer.
 	 */

}
