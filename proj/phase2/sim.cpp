/*
  Justin Cano
  860945660
  John Ericta
  860803073
  CS 177 Winter 2014
  We hereby certify that all of the work done in this submission is entirely our own.
*/
#include <iostream>
#include <cpp.h>
#include <string.h>
#include <math.h>

using namespace std;

int BETA = 15;			// expntl mean, in minutes. arrival rate = 1/BETA

const int NUM_FLOORS = 21;		// number of floors, including lobby (indexed at 0)

const double RECONFIGURE_TIME = 0.0833; // time to reconfigure portal in minutes (5 seconds)
double loadTime() { return uniform(5,15)/60; } // function to return load/unload time

const int MAX_EVENTS = max_events(150);			// change max number of events
event_set want_floor("want_floor", NUM_FLOORS); // floor destination button
event wakeup("wakeup"); //wake up portal if sleeping

facility update_workload("update_workload");
facility rest("rest");			// dummy facility indicaing an idle portal car

event_set portal_open("portal", NUM_FLOORS);
					// event that tells which floor the portal is open at
					// the portal is open at some floor and lobby

/*** Passenger Processes ***/
void incoming();
void incomingPassenger();
void outgoing();
void outgoingPassenger();
void interfloor();
void interfloorPassenger();

/*** Portal Process ***/
void portal();

/*** Helper Functions ***/
int nextCalledFloor(int fromFloor);

/*** Data Table ***/
qtable *qIncomingPassenger = new qtable("incoming passenger");
qtable *qOutgoingPassenger = new qtable("outgoing passenger");
qtable *qInterfloorPassenger = new qtable("interfloor passenger");
table *tIncomingWaitTime = new table("portal wait time for incoming passengers");
table *tOutgoingWaitTime = new table("portal wait time outgoing passengers");
table *tInterfloorWaitTime = new table("portal wait time for interfloor passengers");

extern "C" void sim()			// main process
{
	create("sim");
	cout << "Enter value for BETA: ";
	cin >> BETA;
	incoming();
	outgoing();
	interfloor();
	portal();
	/*while( clock < 720 )
	{
		cout << "********************************************\n";
		cout << "Time: " << clock << endl;
		hold(15);
		report_qtables();
	}*/
	hold(720);			// hold for 720 minutes (12 hours)
	cout << endl << endl << "BETA = " << BETA << endl << "Arrival Rate = 1/BETA = " << 1/static_cast<double>(BETA) << endl << endl;
	report();
}

void incoming()
{
	create("incoming");
	while( clock < 720 )
	{
		hold(expntl(BETA));
		qIncomingPassenger->note_entry();
		incomingPassenger();
	}
}

void incomingPassenger()
{
	create("incomingPassenger");

	int currentFloor = 0; // 0 for lobby
	// I.
	int departureFloor = uniform(1,NUM_FLOORS-1);
	update_workload.reserve();
	want_floor[departureFloor].set();	// tell the portal where you want to go
	wakeup.set();			// wake up portal
	update_workload.release();

	// II.
	double start = clock;
	portal_open[departureFloor].wait(); // wait for portal to connect to departure floor from lobby
	double waitTime = clock-start;
	tIncomingWaitTime->record(waitTime);
}

void outgoing()
{
	create("outgoing");
	while( clock < 720 )
	{
		hold(expntl(BETA));
		qOutgoingPassenger->note_entry();
		outgoingPassenger();
	}
}

void outgoingPassenger()
{
	create("outgoingPassenger");

	int currentFloor = uniform(1,NUM_FLOORS-1);
	// I.
	int departureFloor = 0;
	update_workload.reserve();
	want_floor[currentFloor].set();	// tell the portal to open at current floor so we can go to lobby
	wakeup.set();			// wake up portal
	update_workload.release();

	// II.
	double start = clock;
	portal_open[currentFloor].wait(); //wait for portal to connect to lobby
	double waitTime = clock-start;
	tOutgoingWaitTime->record(waitTime);
}

void interfloor()
{
	create("interfloor");
	while( clock < 720 )
	{
		hold(expntl(BETA));
		qInterfloorPassenger->note_entry();
		interfloorPassenger();
	}
}

void interfloorPassenger()
{
	create("interfloorPassenger");
	int currentFloor = uniform(1,NUM_FLOORS-1);
	int departureFloor = uniform(1,NUM_FLOORS-1);
	while( departureFloor == currentFloor )
		departureFloor = uniform(1,NUM_FLOORS-1);

	// first, we must go to the lobby
	// I.
	update_workload.reserve();
	want_floor[currentFloor].set();	// tell the portal to open at current floor so we can go to lobby
	wakeup.set();			// wake up portal
	update_workload.release();

	// II.
	double start = clock;
	portal_open[currentFloor].wait(); //wait for portal to connect to lobby

	// now we are in lobby
	// III.
	update_workload.reserve();
	want_floor[departureFloor].set();	// tell the portal where you want to go
	wakeup.set();			// wake up portal
	update_workload.release();

	// IV.
	portal_open[departureFloor].wait(); // wait for portal to connect to departure floor from lobby
	double waitTime = clock-start;
	tInterfloorWaitTime->record(waitTime);
}





void portal()
{
	create("portal");
	int currentFloor = 0;
	int whatFloor = 0;
	int lobby = 0;
	int topFloor = NUM_FLOORS-1;
	int openTime = loadTime();
	while(1)
	{
		// start off in idle state, waiting for the first call...
		// should check here that portal is indeed empty!
		rest.reserve();
		wakeup.wait();
		rest.release();

		whatFloor = nextCalledFloor(lobby);

		// iterate through floors until there we reach top floor
		while( whatFloor != -1 )
		{
			hold(RECONFIGURE_TIME); // hold for the time it takes for portal to make a connection

			portal_open[whatFloor].set(); // portal says which floor its connected to
			hold(openTime); // for now, hold the portal open for some time
			portal_open[whatFloor].clear();
			want_floor[whatFloor].clear();

			whatFloor = nextCalledFloor(whatFloor); // what floor do we go to next?
		}
		// reached top floor, loop again
	}
}

// on success, returns the next departure floor that has been called. else, return -1
int nextCalledFloor(int fromFloor)
{
	for( int i = fromFloor; i < NUM_FLOORS; i++ )
	{
		if( want_floor[i].state() == OCC )
			return i;
	}
	return -1;
}
