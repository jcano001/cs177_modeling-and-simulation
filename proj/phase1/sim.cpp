/*
  Justin Cano
  860945660
  John Ericta
  860803073
  CS 177 Winter 2014
  We hereby certify that all of the work done in this submission is entirely our own.
*/
#include <iostream>
#include <cpp.h>
#include <string.h>
#include <math.h>

using namespace std;

int BETA = 15;			// expntl mean, in minutes. arrival rate = 1/BETA

const int NUM_FLOORS = 21;		// number of floors, including lobby (indexed at 0)

const double OPEN_TIME = 0.033;		// time to open door in minutes (2 seconds)
const double CLOSE_TIME = 0.05;		// time to close door in minutes (3 seconds)
double loadTime() { return uniform(5,15)/60; } // function to return load/unload time
double travelTime( int floor1, int floor2 ) // function to return the travel time between floor1 and floor2
{ 
	double param = sqrt(abs(floor2-floor1));
	return (5*param)/60;	
}

const int MAX_EVENTS = max_events(150);			// change max number of events
event_set want_up("want_up", NUM_FLOORS); // elevator UP buttons (no need for an UP button on top floor)
event_set want_dn("want_dn", NUM_FLOORS); // elevator DN buttons (no need for a DN button in lobby)
event_set want_off("want_off", NUM_FLOORS); // floor destination button
event wakeup("wakeup"); //wake up elevator if sleeping

facility update_workload("update_workload");
facility rest("rest");			// dummy facility indicaing an idle elevator car

event_set going_up("going_up", NUM_FLOORS); // elevator opens on a floor so that people who want to go up can go in
event_set going_down("going_down", NUM_FLOORS);
					// elevator opens on a floor so that people who want to go down can go in
event_set here_is_floor("here_is_floor", NUM_FLOORS); // elevator arrives event for every floor


/*** Passenger Processes ***/
void incoming();
void incomingPassenger();
void outgoing();
void outgoingPassenger();
void interfloor();
void interfloorPassenger();

/*** Elevator Process ***/
void elevator();


/*** Helper Functions ***/
int nextCalledFloorUp(int fromFloor);
int nextDestinationFloorUp(int fromFloor);
int nextFloorUp(int fromFloor);
int nextCalledFloorDown(int fromFloor);
int nextDestinationFloorDown(int fromFloor);
int nextFloorDown(int fromFloor);

/*** Data Table ***/
qtable *qIncomingPassenger = new qtable("incoming passenger");
qtable *qOutgoingPassenger = new qtable("outgoing passenger");
qtable *qInterfloorPassenger = new qtable("interfloor passenger");
table *tIncomingWaitTime = new table("elevator wait time for incoming passengers");
table *tOutgoingWaitTime = new table("elevator wait time outgoing passengers");
table *tInterfloorWaitTime = new table("elevator wait time for interfloor passengers");

extern "C" void sim()			// main process
{
	create("sim");
	cout << "Enter value for BETA: ";
	cin >> BETA;
	incoming();
	outgoing();
	interfloor();
	elevator();
	hold(720);			// hold for 720 minutes (12 hours)
	cout << endl << endl << "BETA = " << BETA << endl << "Arrival Rate = 1/BETA = " << 1/static_cast<double>(BETA) << endl << endl;
	report();
}

void incoming()
{
	create("incoming");
	while( clock < 720 )
	{
		hold(expntl(BETA));
		qIncomingPassenger->note_entry();
		incomingPassenger();
	}
}

void incomingPassenger()
{
	create("incomingPassenger");

	int currentFloor = 0; // 0 for lobby
	// I.
	int departureFloor = uniform(1,NUM_FLOORS-1);
	update_workload.reserve();
	want_up[currentFloor].set();	// tell the elevator what floor you're on
	wakeup.set();			// wake up elevator
	update_workload.release();

	// II.
	double start = clock;
	going_up[currentFloor].wait(); // wait for elevetor to come to lobby
	double waitTime = clock-start;
	tIncomingWaitTime->record(waitTime);
	
	// III.
	update_workload.reserve();
	want_off[departureFloor].set();	// tell the elevator where you want to get off
	update_workload.release();

	// IV.
	here_is_floor[departureFloor].wait();
}

void outgoing()
{
	create("outgoing");
	while( clock < 720 )
	{
		hold(expntl(BETA));
		qOutgoingPassenger->note_entry();
		outgoingPassenger();
	}
}

void outgoingPassenger()
{
	create("outgoingPassenger");

	int currentFloor = uniform(1,NUM_FLOORS-1);
	// I.
	int departureFloor = 0;
	update_workload.reserve();
	want_dn[currentFloor].set();	// tell the elevator what floor you're on
	wakeup.set();			// wake up elevator
	update_workload.release();

	// II.
	double start = clock;
	going_down[currentFloor].wait(); //wait for elevator to come to your floor
	double waitTime = clock-start;
	tOutgoingWaitTime->record(waitTime);

	// III.
	update_workload.reserve();
	want_off[departureFloor].set();	// tell the elevator where you want to get off
	update_workload.release();

	// IV.
	here_is_floor[departureFloor].wait();
}

void interfloor()
{
	create("interfloor");
	while( clock < 720 )
	{
		hold(expntl(BETA));
		qInterfloorPassenger->note_entry();
		interfloorPassenger();
	}
}

void interfloorPassenger()
{
	create("interfloorPassenger");

	int currentFloor = uniform(1,NUM_FLOORS-1);
	int departureFloor = uniform(1,NUM_FLOORS-1);
	while( departureFloor == currentFloor )
		departureFloor = uniform(1,NUM_FLOORS-1);

	double start = 0;
	// determine if passenger wants to go up or down
	if( (currentFloor-departureFloor) < 0 ) // passenger wants to go up
	{
		// I.
		update_workload.reserve();
		want_up[currentFloor].set();	// tell the elevator what floor you're on
		wakeup.set();			// wake up elevator
		update_workload.release();

		// II.
		start = clock;
		going_up[currentFloor].wait();	// wait for elevator to come to your floor
	}
	else // passenger wants to go down
	{
		// I.
		update_workload.reserve();
		want_dn[currentFloor].set();	// tell the elevator what floor you're on
		wakeup.set();			// wake up elevator
		update_workload.release();

		// II.
		start = clock;
		going_down[currentFloor].wait();	// wait for elevator to come to your floor
	}
	double waitTime = clock-start;
	tInterfloorWaitTime->record(waitTime);

	// III.
	update_workload.reserve();
	want_off[departureFloor].set();	// tell the elevator where you want to get off
	update_workload.release();

	// IV.
	here_is_floor[departureFloor].wait();
}





void elevator()
{
	create("elevator");
	int currentFloor = 0;
	int whatFloor = 0;
	int lobby = 0;
	int topFloor = NUM_FLOORS-1;
	while(1)
	{
		// start off in idle state, waiting for the first call...
		// should check here that elevator is indeed empty!
		rest.reserve();
		wakeup.wait();
		rest.release();
		wakeup.clear();

		whatFloor = nextCalledFloorUp(lobby);

		// go around loop until there are no calls waiting
		// first let's go up
		while( whatFloor != -1 )
		{
			hold(travelTime(whatFloor,currentFloor)); // hold for the time it takes for elevator to travel
			currentFloor = whatFloor;
			here_is_floor[currentFloor].set();

			hold(OPEN_TIME);
			going_up[currentFloor].set();
			hold(loadTime());
			hold(CLOSE_TIME);
			going_up[currentFloor].clear();	

			here_is_floor[currentFloor].clear();
			want_up[currentFloor].clear();
			want_off[currentFloor].clear();

			whatFloor = nextFloorUp(currentFloor); // what floor do we go to next?
		}

		// now let's go down
		whatFloor = nextCalledFloorDown(topFloor);
		while( whatFloor != -1 )
		{
			hold(travelTime(whatFloor,currentFloor)); // hold for the time it takes for elevator to travel
			currentFloor = whatFloor;
			here_is_floor[currentFloor].set();

			hold(OPEN_TIME);
			going_down[currentFloor].set();
			hold(loadTime());
			hold(CLOSE_TIME);
			going_down[currentFloor].clear();	

			here_is_floor[currentFloor].clear();
			want_dn[currentFloor].clear();
			want_off[currentFloor].clear();

			whatFloor = nextFloorDown(currentFloor); // what floor do we go to next?
		}
	}
}

// on success, returns the next floor up that has been called. else, return -1
int nextCalledFloorUp(int fromFloor)
{
	for( int i = fromFloor; i < NUM_FLOORS; i++ )
	{
		if( want_up[i].state() == OCC )
			return i;
	}
	return -1;
}

//on success, returns next destination floor up. else, return -1
int nextDestinationFloorUp(int fromFloor)
{
	for( int i = fromFloor; i < NUM_FLOORS; i++ )
	{
		if( want_off[i].state() == OCC )
			return i;
	}
	return -1;
}

//on success, return the first floor up to stop at. else, return -1
int nextFloorUp(int fromFloor)
{
	int calledFloor = nextCalledFloorUp(fromFloor);
	int destinationFloor = nextDestinationFloorUp(fromFloor);

	if( (calledFloor == -1) && (destinationFloor == -1) )
		return -1;
	if( calledFloor == -1 )
		return destinationFloor;
	else if( destinationFloor == -1 )
		return calledFloor;
	return -1;
}

//on success, returns the next floor down that has been called. else, return -1
int nextCalledFloorDown(int fromFloor)
{
	for( int i = fromFloor; i >= 0; i-- )
	{
		if( want_dn[i].state() == OCC )
			return i;
	}
	return -1;
}

//on success, returns next destination floor down. else, return -1
int nextDestinationFloorDown(int fromFloor)
{
	for( int i = fromFloor; i >= 0; i-- )
	{
		if( want_off[i].state() == OCC )
			return i;
	}
	return -1;
}

//on success, returns the first floor down to stop at. else, return -1
int nextFloorDown(int fromFloor)
{
	int calledFloor = nextCalledFloorDown(fromFloor);
	int destinationFloor = nextDestinationFloorDown(fromFloor);

	if( (calledFloor == -1) && (destinationFloor == -1) )
		return -1;
	if( calledFloor == -1 )
		return destinationFloor;
	else if( destinationFloor == -1 )
		return calledFloor;
	return -1;
}
