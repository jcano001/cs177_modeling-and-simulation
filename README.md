# CS 177 - Modeling and Simulation
## UCR | Winter 2014
>Covers validation of random number sequences; concepts in modeling and systems analysis; and conceptual models and their mathematical and computer realizations. Examines simulation modeling techniques, including object-oriented modeling and discrete-event modeling. Emphasizes the use of simulation libraries used with programming languages such as C++.
>All programs have been coded in either C++ or CSIM
